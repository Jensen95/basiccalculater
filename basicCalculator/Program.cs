﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace basicCalculator
{
    class Frame
    {
        public static void Answere(decimal answere)
        {
            string message = "The answere to that would be: " + answere;
            int pad = 2;
            int rows = pad * 2 + 3;
            int cols = message.Length + pad * 2 + 2;

            Console.WriteLine();

            for (var r = 0; r != rows; r++)
            {
                for (var c = 0; c != cols; c++)
                {
                    if (r == pad + 1 && c == pad + 1)
                    {
                        Console.Write(message + ' ');
                        c += message.Length;
                    }
                    else
                    {
                        if (r == 0 || r == rows - 1 || c == 0 || c == cols - 1)
                        {
                            Console.Write('*');
                        }
                        else
                        {
                            Console.Write(' ');
                        }
                    }
                }
                Console.WriteLine();
            }
        }
        public static void Text(string text)
        {
            int pad = 2;
            int rows = pad * 2 + 3;
            int cols = text.Length + pad * 2 + 2;

            Console.WriteLine();

            for (var r = 0; r != rows; r++)
            {
                for (var c = 0; c != cols; c++)
                {
                    if (r == pad + 1 && c == pad + 1)
                    {
                        Console.Write(text + ' ');
                        c += text.Length;
                    }
                    else
                    {
                        if (r == 0 || r == rows - 1 || c == 0 || c == cols - 1)
                        {
                            Console.Write('*');
                        }
                        else
                        {
                            Console.Write(' ');
                        }
                    }
                }
                Console.WriteLine();
            }
            Console.WriteLine();
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            string name, operation;
            Int32 age;
            //Initialising the oprands list
            List<decimal> oprands = new List<decimal>();
            for (var i = 0; i < 2; i++)
            {
                oprands.Add(0);
            }


            //Asking for the users name and age
            Console.Write("Please enter your name: ");
            name = Console.ReadLine();
            Console.Write("Thank you {0}, now please enter your age: ", name);

            //Checking if the age input was a number
            bool b = false;
            do
            {
                string s = Console.ReadLine();
                if(Int32.TryParse(s,out age))
                {
                    //Set b to true if succeeded 
                    b = true;
                    //Checking age
                    if (age > 100)
                    {
                        Console.WriteLine("You don't look that old!");
                        System.Threading.Thread.Sleep(2000);
                    }
                }
                else
                {
                    Console.Write("You didn't type in a number, try again! ");
                }
            } while (b != true);

            //Clear the screen and greet
            Console.Clear();
            Frame.Text("Hello " + name + ", your age is set to: " + age);

            //Making sure that the user can make all the calculations the person desire
            do
            {
                //Get the two inputs
                for (var i = 0; i < oprands.Count(); i++)
                {
                    Console.Write("Type the {0}. number: ", i+1);
                    var j = 0;
                    while(j < 1)
                    {
                        decimal k;
                        string s = Console.ReadLine();
                        if (Decimal.TryParse(s, out k))
                        {
                            oprands[i] = (k);
                            j++;
                            Console.WriteLine("");
                        }
                        else
                        {
                            Console.Write("It needs to be a number! ");
                        }
                    }
                }
                //Failsafe if the user doesn't type correctly
                do
                {
                    Console.WriteLine("Wich operation do you want?");
                    Console.Write("+, -, *, /: ".PadLeft(2));
                    operation = Console.ReadLine();
                    //Switch for choosing the desired calculation
                    switch (operation)
                    {
                        case "1":
                        case "+":
                            Frame.Answere(oprands[0] + oprands[1]);
                            b = true;
                            break;
                        case "2":
                        case "-":
                            Frame.Answere(oprands[0] - oprands[1]);
                            b = true;
                            break;
                        case "3":
                        case "*":
                            Frame.Answere(oprands[0] * oprands[1]);
                            b = true;
                            break;
                        case "4":
                        case "/":
                            //Check that the second value isn't 0
                            if(oprands[1] == 0)
                            {
                                Console.WriteLine("Nope, chuck testa");
                                Console.WriteLine("You can't divide with 0");
                            }
                            else
                            {
                                Frame.Answere(oprands[0] / oprands[1]);
                            }
                            
                            b = true;
                            break;
                        default:
                            Console.Write("You didn't choose one of the options available, try again");
                            b = false;
                            break;
                    } 
                } while (b != true);

                //Ask if the user have another question
                Console.WriteLine();
                Console.Write("Do you have more calculations? ");
                string calculations = Console.ReadLine();
                calculations = calculations.ToLower();
                calculations = calculations.Trim('e', 's');
                if (calculations != "y")
                {
                    b = false;
                }
                else
                {
                    Console.Clear();
                    Frame.Text("Hello " + name + ", your age is set to: " + age);

                }
            } while (b == true);
        }
    }
}
